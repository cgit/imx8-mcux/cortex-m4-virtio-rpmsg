#CPPFLAGS=-g -pthread -I/sw/include/root
CPPFLAGS=-g
LDFLAGS=-g
LDLIBS=
RM=rm -f

CROSS_COMPILE_ARM64=aarch64-linux-gnu-gcc
CROSS_COMPILE_ARMHF=arm-linux-gnueabihf-gcc

SRCS=cm4-virtio-test.c
OBJS=$(subst .c,.o,$(SRCS))

all: cm4-virtio-test

# Linker
cm4-virtio-test: cm4-virtio-test.o
	$(CROSS_COMPILE_ARM64) $(LDFLAGS) -o cm4-virtio-test cm4-virtio-test.o $(LDLIBS)

# Compiler
cm4-virtio-test.o: cm4-virtio-test.c
	$(CROSS_COMPILE_ARM64) $(CPPFLAGS) -c cm4-virtio-test.c

.PHONY: clean

clean:
	$(RM) $(OBJS)
	$(RM) cm4-virtio-test
